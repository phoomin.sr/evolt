@extends('layouts.apps')

@section('style')

@endsection

@section('contents')
    <div class="container">
        <div class="row mb-3 mt-3">
            <div class="col">
                @auth('admin')
                    <h1 class="title">Evolt framwork</h1>
                @endauth
                @auth('sale')
                    <h1 class="title">the Best framework</h1>
                @endauth
                @auth('customer')
                    <h1 class="title">fit for your business</h1>
                @endauth
                @guest
                    <h1 class="title">Cola-engine</h1>
                @endguest
            </div>
        </div>
        <div class="row">
            <div class="col">
                <h4>this's mvc framework create by minostra</h4>
                <p class="text-break">
                    powered engine with <b>Jquery</b> and <b>bootstrap</b>. 
                    iconpack default with free package <b>font-awesome</b>.
                    this test for use <b>Fetch</b> for send HTTP Request for user save with post method.
                    however, we can create class user for manage server behavior by request method.
                </p>
                <div class="row">
                    <div class="col"></div>
                    <div class="col">
                        <p class="text-muted text-end">-- minostra</p>
                    </div>
                </div>
            </div>
            @auth('admin')
                <div class="col-3">
                    <div class="row mb-3">
                        <div class="col bg-light p-2">
                            <button class="btn btn-secondary rounded" data-source='viewtype' data-target='table'><i class="fas fa-table"></i></button>
                            <button class="btn btn-secondary rounded" data-source='viewtype' data-target="list"><i class="fas fa-th-list"></i></button>
                            <input type="hidden" name="user_id" value="1">
                        </div>
                    </div>
                    <div class="row show-panel">
                        @if ($show['viewstyle'] == 'table')
                            <table class="table">
                                <tr>
                                    @foreach ($head as $item)
                                        <th>{{ $item }}</th>
                                    @endforeach
                                </tr>
                                @for ($i = 0; $i < count($recs); $i++)
                                    <tr>
                                        <td><p data-edit='true' data-id="{{ $recs[$i]['id'] }}" data-target="name">{{ $recs[$i]['name'] }}</p></td>
                                        <td><p data-edit='true' data-id="{{ $recs[$i]['id'] }}" data-target="create_at">{{ $recs[$i]['create_at'] }}</p></td>
                                        <td><input type="button" data-id="{{ $recs[$i]['id'] }}" value="ลบ" class="btn btn-block btn-danger"></td>
                                    </tr>
                                @endfor
                            </table>  
                        @else
                            @for ($i = 0; $i < count($recs); $i++)
                                <div class="card mb-2">
                                    <div class="card-header">
                                        {{ $recs[$i]['name'] }}
                                    </div>
                                    <div class="card-body">
                                        <blockquote class="blockquote mb-0">
                                            <footer class="blockquote-footer">created at <cite title="Source Title">{{ $recs[$i]['create_at'] }}</cite></footer>
                                        </blockquote>
                                    </div>
                                </div>
                            @endfor
                        @endif
                    </div>
                </div>
            @endauth
        </div>
    </div>
@endsection

@section('script')
    <script>
        $(document).ready(function () {
            $('[name=rangeDatewFW]').daterangepicker({
                opens: 'left',
                startDate: moment(),
                endDate: moment().endOf('month')
            }, function () {
                console.log();
            });
            // auto save form
            $('button[data-source]').click(function () {
                user_id = $('input[type=hidden][name=user_id]').val();
                target = $(this).data('target');

                send('./user', 'post', {
                    user_id: user_id,
                    viewstyle: target
                })
                .then(res => {
                    $('.show-panel').html(res);
                });
            });

            $('p').dblclick(function () {
                let val = $(this).text();
                let id = $(this).attr('data-id');
                let target = $(this).attr('data-target');
                $(this).html("<input type='text' name='"+target+"' data-id='"+id+"' value='"+val+"' class='form-control'>");
                wait_for_submit($(this));
            });

            $('input[type=button].btn-danger').click(function () {
                let id = $(this).attr('data-id');
                send('./user', 'delete', {
                    id: id
                })
                .then(res => {
                    if (!res.error) {
                        window.location.reload();
                    }
                });
            });

            function wait_for_submit(ele) {
                $('input[type=text]').select();
                $('input[type=text]').focusout(function () {
                    send('./user', 'update', {
                        value: $(this).val(),
                        column: $(this).attr('name'),
                        id: $(this).attr('data-id')
                    }).then(res => {
                        $('.show-panel').html(res);
                        window.location.reload();
                    });
                });
            }
        });
        function submit_add_user(data, url = "") {
            send(url, 'post', data)
            .then(res => {
                $('.show-panel').html(res);
            })
        }
    </script>
@endsection