<?php

use providers\request\Request;
use providers\view\Views;

$router->get('/', [controllers\home::class, 'index']);
$router->any('user', [controllers\user::class]);

$router->get('test/{name}?', function ($name) {
    return 'hello world mr.' . $name;
});

$router->group(['prefix' => 'admin'], function ($router) {
    $router->get('showname/{name}?', [controllers\admin::class]);
});

$router->group(['prefix' => 'api'], function ($router) {
    $router->post('showusername', [controllers\user::class, 'showname']);
});
