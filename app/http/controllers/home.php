<?php namespace controllers;

use app\database\user;
use app\database\usermeta;
use providers\view\Views;
use models\base;

class home {
    public function index() 
    {
        $user = new user;
        $allUser = $user->select('id', 'name', 'create_at')->get();
        
        return Views::render('home', array())->Auth('admin', 'admin');
    }
}