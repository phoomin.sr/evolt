<?php namespace controllers;

use controllers\base;
use providers\request\Request;
use providers\view\Views;

class admin extends base {
    public function get($name)
    {
        return Views::render(
            'home',
            [
                'name' => $name
            ]
        );
    }
}