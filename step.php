#!/usr/bin/php
<?php
require __DIR__ . "/vendor/autoload.php";

use splitbrain\phpcli\CLI;
use splitbrain\phpcli\Options;

class OneStep extends CLI
{
    protected function setup(Options $options)
    {
        $options->setHelp('A very minimal example that does nothing but print a version');
        $options->registerOption('version', 'print version', 'v');
        $options->registerOption('migrate', 'php migration to mysql database', 'm');
        $options->registerOption('create', 'php create table to mysql', 'c');
    }
    protected function main(Options $options)
    {
        if ($options->getOpt('version')) {
            $this->info('1.0.0');
        } else if ($options->getOpt('migrate')) {
            $this->warning('this is migrate');
        } else if ($options->getOpt('create')) {
            $server = new server\serve;
            $this->info($server->output);
        } else {
            echo $options->help();
        }
    }
}

$cli = new OneStep;
$cli->run();